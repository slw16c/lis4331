
import java.util.ArrayList;

public class ArraysAndLoops {
 
 
    public static void main(String[] args) {
   
        //Declare Variables
        int wloop = 0;
        int dwloop = 0;
        String animals[] = {"dog", "cat", "bird", "fish", "insect"};

        System.out.println("Program loops through array of strings");
        System.out.println("Use following values: dog, cat, bird, fish, insect.");
        System.out.println("Use following loop structures: for, enhanced for, while, do...while.\n");

        System.out.println("Program loops through array of strings\n");

        
        System.out.println("for loop:");
        
            for(int i = 0; i < animals.length; i++) {
            System.out.println(animals [i]);
            }


        System.out.println("\nenhanced for loop:");

            for (String word: animals) {
                System.out.println(word);
            }

        
        System.out.println("\nwhile loop:");

            while ( wloop < animals.length) {
                System.out.println(animals[wloop]);
                wloop++;
            }


        System.out.println("\ndo...while loop:");

            do {
                System.out.println(animals[dwloop]);
                dwloop++;
            }
            while (dwloop < animals.length);

    

    }//End of main
}//End of class