import java.util.Scanner;

public class SimpleInterestCalculator {
    public static void main(String[] args) {

       
        boolean lock = true;
        boolean lock2 = true;
        boolean lock3 = true;
        Scanner input = new Scanner(System.in);
        double num1 = 0;
        double num2 = 0;
        double num3 = 0;
        double num4 = 0;
        double save = 0;
        double save2 = 0;
        String firstInput = "";
        String secondInput = "";
        String thirdInput = "";


    System.out.println("Program swaps two integers. \nNote: Program checks for integers and non-numeric values.\n");


    System.out.print("Current principal: $");

    while (lock == true){

        firstInput = input.next();

        try {
            num1 = Double.parseDouble(firstInput);
            lock = false;
        } catch (Exception e) {
            System.out.print("Not valid number!\n");
            System.out.print("Please try again. Enter principal: $");
            lock = true;
        }
    }

    System.out.print("Interest Rate (per year): ");

    while (lock2 == true){

        secondInput = input.next();

        try {
            num2 = Double.parseDouble(secondInput);
            lock2 = false;
        } catch (Exception e) {
            System.out.print("Not valid number!\n");
            System.out.print("Please try again. Enter interest rate: ");
            lock2 = true;
        }
    }
    
    System.out.print("Total time (in years): ");

    while (lock3 == true){

        thirdInput = input.next();

        try {
            num3 = Integer.parseInt(thirdInput);
            lock3 = false;
        } catch (Exception e) {
            System.out.print("Not valid integer!\n");
            System.out.print("Please try again. Enter years: ");
            lock3 = true;
        }
    }

    num4 = num2 / 100;
    save = (num4 * num1) * num3;
    save2 = num1 + save;
    
    System.out.println("You will have saved $%.2f" + save2 + "in " + num3 + " years, at an interest rate of " + num2 + "%");


    }//end of main
}//end of NumberSwap
