import java.util.Scanner;

public class LargestNumber {
 
 
    public static void main(String[] args) {
   
        //Declare Variables

        int num1;
        int num2;

        //Declare Scanner object

        Scanner input = new Scanner(System.in);


        System.out.println("Program evaluates largest of two integers." );
        System.out.println("Note: Program does *not* check for non-numeric characters or non-integer values." );

        
        //Asks the user to enter two integers

        System.out.println("Enter first integer");
        num1=input.nextInt();//Reads the input from user

        System.out.println("Enter second integer");
        num2=input.nextInt();//Reads the input from user

        //Checks to see which number is the Larger number

        if(num1 > num2){
            System.out.println(num1 + " " + "is larger than" + " " + num2);
        }else if(num1 < num2){
            System.out.println(num2 + " " + "is larger than" + " " + num1);
        }else {
            System.out.println(num1 + " " + "and" + " " + num2 + " " + "are equal" );
        }

    }//End of main
}//End of class