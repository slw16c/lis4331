import java.util.Scanner;

public class Ascii {
    public static void main(String[] args) {

        int i;
        boolean lock = true;
        Scanner input = new Scanner(System.in);
        double asciiVal = 0;
        String inputText = "";
    
    System.out.println("Printing characters A-Z as ASCII values: ");
        
    for (i = 65; i<= 90; i++) {
        char ch = (char) i;
        System.out.println("Character " + ch +" has ascii value " + i);
    }
    
    System.out.println("\nPrinting ASCII values 48-122 s characters:");
    for (i = 65; i<= 122; i++) {
        char ch = (char) i;
        System.out.println("ASCII value " + i +" has character value " + ch);

    }

    System.out.println("Allowing user ASCII value input: ");

    while (lock == true){

        System.out.print("Please enter ASCII value (32 - 127): ");
        inputText = input.next();

        try {
            asciiVal = Double.parseDouble(inputText);
            lock = false;
        } catch (Exception e) {
            System.out.print("Invalid integer--ASCII value must be a number\n");
            lock = true;
        }

        if (  ( asciiVal < 32  || asciiVal > 127 ) && (lock == false)   ) {
            System.out.print("ASCII value must be >= 32 or <= 127\n");
            lock = true;
        }
    }

    char asc = (char) asciiVal;
    System.out.println("ASCII value " + asciiVal +" has character value " + asc);



    }//end of main
}//end of Ascii
