import java.util.Scanner;

public class TravelTime {
    public static void main (String[] args){

        //Control Varible
        String inputText = "",
               contApp = "Y";
        double milesInput = 0.0,
               mph = 0.0;
        int    finalHours = 0,
               finalMinutes = 0;

        Scanner input = new Scanner(System.in);

        while (contApp.equals("Y")){
            
            //Locking Vaibles
            boolean lock1 = true,    //false for minutes, true for horus
                    lock2 = true;

            System.out.print("\nPlease enter miles: ");
            inputText = input.next();

            while (lock1 == true){
                try {
                    milesInput = Double.parseDouble(inputText);
                    lock1 = false;
                } catch (Exception e) {
                    System.out.print("Invalid double--miles must be a number.\n\n");
                    System.out.print("Please enter miles: ");
                    lock1 = true;
                    inputText = input.next();
                }

                if (( milesInput > 3000  || milesInput < 0 ) && (lock1 == false)) {
                    System.out.print("Miles must be greater than 0, and no more than 3000.\n\n");
                    System.out.print("Please enter miles: ");
                    lock1 = true;
                    inputText = input.next();
                }
            }
            
            System.out.print("\nPlease enter MPH: ");
            inputText = input.next();

            while (lock2 == true){
                try {
                    mph = Double.parseDouble(inputText);
                    lock2 = false;
                } catch (Exception e) {
                    System.out.print("Invalid double--MPH must be a number.\n\n");
                    System.out.print("Please enter MPH: ");
                    lock2 = true;
                    inputText = input.next();  
                }

                if (( mph > 100 || mph < 0) && (lock2 == false)) {
                    System.out.print("MPH must be greater than 0, and no more than 100.\n\n");
                    System.out.print("Please enter MPH: ");
                    lock2 = true;
                    inputText = input.next();
                }
            }

            finalHours = RetriveTime(milesInput , mph , false);
            finalMinutes = RetriveTime(milesInput , mph , true);

            System.out.print("Estimated travel time: " + finalHours + " Hr(s) ");
            if ( finalMinutes != 0 ) {
                System.out.print(finalMinutes + " Minutes \n\n");
            } else {
                System.out.print("\n\n");
            }
            
            System.out.print("Continue? (y/n): ");
            contApp = input.next();
            contApp = contApp.toUpperCase();
        }
    }

    public static int RetriveTime (double miles , double hours , boolean minOrHour){
        double holdHours = 0.0,
               holdMinutes = 0.0;
        int totalToReturn = 0;
        final double MINUTES = 60.0;

       // Time = distance / speed

        holdHours = ((miles/hours)*MINUTES);
        holdMinutes = holdHours;
        holdHours = Math.floor(holdHours/MINUTES);
        holdMinutes = (holdMinutes % MINUTES);

        if (minOrHour == false) {totalToReturn = (int)holdHours;}
        if (minOrHour == true)  {totalToReturn = (int)holdMinutes;}

        return totalToReturn; 
    }
}