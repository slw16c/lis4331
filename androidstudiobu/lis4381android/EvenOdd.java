import java.util.Scanner;

public class EvenOdd {
 
 
    public static void main(String[] args) {
   
        double number;//Declare Variables

        Scanner input = new Scanner(System.in);//Declare Scanner object

        System.out.println("Enter integer");//Asks the user to Enter an integer
        number=input.nextDouble();//Reads the input from user

        if(number % 2 == 0)
            System.out.println(number + " " + "is an even number." );//If the number entered has a remainder that when divided by 2 equals 0, then the 'if' statement returns even
            else
            System.out.println(number + " " + "is an odd numer");//If the number entered has a remainder that when divided by 2 does NOT equal 0, then the 'else' statement returns odd


    }//End of main
}//End of class