class Product {

    private String code;
    private String des;
    private double price;

    public Product() {

        System.out.println("\nInside product default constructor.");
        code = "abc123";
        des = "My Widget";
        price = 49.99;

    }

    public Product(String code, String des, double price) {

        System.out.println("\nInside product constructor with parameters.");
        this.code = code;
        this.des = des;
        this.price = price; 
    }

    public String getCode() {
        return code;
    }

    public void setCode(String c) {
        code = c;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String d) {
        des = d;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double p) {
        price = p;
    }

    public void print() {
        System.out.print("\nCode:" + code + ", Description: " + des + ", Price: $" + price);
    }
}