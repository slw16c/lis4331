import java.util.Scanner;

public class Circle {
  public static void main(String[] args) {
   
        //Declare variables

        Scanner input = new Scanner(System.in);

        double num = 0;
        double dia; 
        double cir;
        double area;
        boolean validate;

        System.out.println("Non-OOP program calculates diameter, circumference, and circle area.");
        System.out.println("Must use Java's built-in PI constant, printf(), and formatted to 2 decimal places.");
        System.out.println("Must *only* permit numeric entry.");


        //Checking to make sure the user only inputs numbers
        do{
          System.out.print("Enter Radius: ");
          if (input.hasNextDouble()) {
            num = input.nextDouble();
            validate = true;
          }else{
            System.out.print("Please try again. ");
            validate = false;
            input.next();
          }
        }while (!(validate));
        
        //Does mathy stuff
        dia= num*2;
        cir= 2 * Math.PI * num;
        area= Math.PI * (num * num);


        System.out.print("Circle diameter: ");
        System.out.printf("%.2f" , dia);
        System.out.print("\nCircumference: " );
        System.out.printf("%.2f" , cir);
        System.out.print("\nArea: " );
        System.out.printf("%.2f" , area);

  }//end of main
}//end of Circle