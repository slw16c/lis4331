# LIS4331 Advanced Mobile Applications Development

## Steve Walter

### Assignment 1 Requirements:

*two Parts:*

1. Create Mortgage Interest Calculator app
2. Questions

#### README.md file should include the following items:

* Screenshot of running application’s splash screen;
* Screenshot of running main activity
* Screenshot of running application’s invalid screen
* Screenshots of running application’s valid screen

#### Assignment Screenshots:

| *Screenshot of running Splash Screen*   | *Screenshot of Main Activity*    |
|-------------------------------------------------:|:--------------------------------------------------|
| ![Tip Calculator First Activity](img/1.jpg) | ![Tip Calculator Second Activity](img/2.jpg) |

| *Screenshot of invalid screen*   | *Screenshot of valid screen*    |
|-------------------------------------------------:|:--------------------------------------------------|
| ![Tip Calculator First Activity](img/3.jpg) | ![Tip Calculator Second Activity](img/4.jpg) |
