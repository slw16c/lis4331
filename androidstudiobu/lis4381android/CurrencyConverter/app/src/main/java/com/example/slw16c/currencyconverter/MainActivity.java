package com.example.slw16c.currencyconverter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import java.text.NumberFormat;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    Double finalResult = 0.0;
    Double usTotal = 0.0;
    

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText total = (EditText)findViewById(R.id.txtInsert);
        final RadioButton euro = (RadioButton)findViewById(R.id.radEuro);
        final RadioButton pesos = (RadioButton)findViewById(R.id.radPesos);
        final RadioButton canadian = (RadioButton)findViewById(R.id.radCan);
        Button convert = (Button)findViewById(R.id.btnConvert);
        convert.setOnClickListener(new View.OnClickListener() {
            final TextView result = ((TextView)findViewById(R.id.txtResult));
            @Override

            public void onClick(View v) {

                usTotal = Double.parseDouble(total.getText().toString());
                NumberFormat eu = NumberFormat.getCurrencyInstance(Locale.UK);
                NumberFormat us = NumberFormat.getCurrencyInstance(Locale.US);

                if(usTotal<1 || usTotal>100000){

                    Toast.makeText(getApplicationContext(),"US Dollars must be <= 100,000 and > 0",Toast.LENGTH_SHORT).show();

                }else {

                        if(euro.isChecked()){

                        finalResult = usTotal * 0.88;

                        result.setText(eu.format(finalResult)+" Euros");

                        }else if(pesos.isChecked()){

                        finalResult = usTotal * 19.08;

                        result.setText("MXN"+ us.format(finalResult) + " Pesos");

                        }else if(canadian.isChecked()){

                        finalResult = usTotal * 1.33;

                        result.setText("CAD"+ us.format(finalResult) + " Canadian");

                        }
                }
            }
        });

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

    }
}
