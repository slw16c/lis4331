# LIS4331 Advanced Mobile Applications Development

## Steve Walter

### Project 2 Requirements:

*Two Parts:*

1. Create an app that uses SQL to store and display a list
2. Questions

#### README.md file should include the following items:

* Screenshot of running Splash Screen
* Screenshot of running Task List Activity

#### Assignment Screenshots:

| *Screenshot of running Splash Screen*: | *Screenshot of running Task List Activity*: |
|----------:|:---------|
| ![Splash Screen](img/1.jpg) | ![Task List Activity](img/2.jpg) |