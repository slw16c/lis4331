# LIS4331 Advanced Mobile Applications Development

## Steve Walter

### Project 1 Requirements:

*two parts:*

1. Create an app that plays music
2. Questions

#### README.md file should include the following items:

* Screenshot of running Splash Screen
* Screenshot of running Splash Screen
* Screenshot of app play screen with buttons invisible
* Screenshot of app music paused

#### Assignment Screenshots:

| *Screenshot of running Splash Screen*   | *Screenshot of running Splash Screen*    |
|-------------------------------------------------:|:--------------------------------------------------|
| ![Tip Calculator First Activity](img/ss1.jpg) | ![Tip Calculator Second Activity](img/ss2.jpg) |

| *Screenshot of app play screen with buttons invisible*   | *Screenshot of app music paused*    |
|-------------------------------------------------:|:--------------------------------------------------|
| ![Tip Calculator First Activity](img/ss3.jpg) | ![Tip Calculator Second Activity](img/ss4.jpg) |
