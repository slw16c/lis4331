<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio">
	<meta name="author" content="Steve Walter">
	<link rel="icon" href="favicon.ico">

	<title>My Online Portfolio</title>

	<%@ include file="/css/include_css.jsp" %>		

<!-- Carousel styles -->
<style type="text/css">
	body{
		background-color: #ccccb3
	}
	h5
	{
		margin: 0;     
		color: #fff;
		padding-top: 50px;
		font-size: 52px;
		font-family: "trebuchet ms", sans-serif; 
		text-shadow: 2px 2px 4px #000;   
	}
	h2
	{
		margin: 0;     
		color: #ccccb3;
		padding-top: 50px;
		font-size: 52px;
		font-family: "trebuchet ms", sans-serif; 
		text-shadow: 2px 2px 4px #000000;   
	}
	h3
	{
		margin: 0;     
		color: #fff;
		padding-top: 50px;
		font-size: 52px;
		font-family: "trebuchet ms", sans-serif; 
		text-shadow: 2px 2px 4px #000000;   
	}
	h4 > a
	{
		color: #fff;
		font-size: 30px;
		text-shadow: 2px 2px 4px #000;
	}
	p
	{
		font-size: 30px;
		text-shadow: 2px 2px 4px #000000;
	}
	.item
	{
		background: #333;
		text-align: center;
		height: 300px !important;
	}
	.carousel
	{
	  margin: 20px 0px 20px 0px;
	}
	.bs-example
	{
	  margin: 1px 10px;
	}
	.carousel-inner > .item > img {
	  position: cover;
	  top: 0;
	  left: 0;
	  height: 100%;
	}
	</style>
	
</head>
<body>
	
	<%@ include file="/global/nav_global.jsp" %>	
	
	<div class="container">
		 <div class="starter-template">
						<div class="page-header">
						<%@ include file="/global/header.jsp" %>							
						</div>

<!-- Start Bootstrap Carousel  -->
<div class="bs-example">

    <div id="myCarousel" 
        class="carousel" 
        data-ride="carousel"
        data-interval="100000"
		data-pause="hover"
		data-wrap="true"
		data-keyboard="true"			
		data-ride="carousel">

    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">

        <div class="item active" >

            <img class="first-slide" src="img/slide1.png" alt="First slide">

            <div class="carousel-caption">
                <h5>Visit My BitBucket</h5>
                <h4><a href="http://bitbucket.org/slw16c/lis4368/">Steve Walter Bitbucket</a></h4>
                
                <img src="img/bitbucket-logo-black-and-white.png" alt="Steve Walter" style="height:100px;" style="width:100px;">
            </div>
        </div>

        <div class="item">

            <img class="first-slide" src="img/linkedin.png" alt="Second slide">

            <div class="carousel-caption">
                <h2>Visit My Linkedin Account!</h2>
                <p><a href="https://www.linkedin.com/in/steven-walter-9b339249/">Steve Walter Linkedin</a></p>
                
                <img src="img/In-Black-101px-R.png" alt="Steve Walter" style="height:100px;" style="width:100px;">
            </div>
        </div>

        <div class="item">

            <img class="first-slide" src="img/slide3.png" alt="Third slide">

            <div class="carousel-caption">
                <h3>Visit a Website I made</h3>
                <p><a href="res/project11/index.html">My VR Website</a></p>
                
                <img src="img/vr.png" alt="Vrtual Reality" style="height:100px;" style="width:100px;">
            </div>
        </div>

    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
        <span class="sr-only">Next</span>
    </a>
    </div>
</div>
<!-- End Bootstrap Carousel  -->

 	<%@ include file="/global/footer.jsp" %>

	</div> <!-- end starter-template -->
</div> <!-- end container -->

 	<%@ include file="/js/include_js.jsp" %>
	
</body>
</html>
