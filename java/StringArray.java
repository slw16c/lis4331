public class StringArray {



  public static void main(String[] args) {

    String array1[] = {"c+", "c", "Java", "Python", "JSON"};
    String array2[] = new String[5];

    for (int i=0; i <= 4; i++){
        array2[i]=array1[i];
    }
    System.out.println("Array with for-loop");
    for (int i=0; i <= 4; i++){
        System.out.println(array1[i]);
    }
    System.out.println("Array with enhanced for-loop");
    for (String array : array2){
        System.out.println(array);
    }
   
  }//end of main
}//end of StringArray