import java.util.Scanner;

class ProductDemo {

    public static void main(String[] args){

        String c = "";
        String d = "";
        double p = 0;
        Scanner input = new Scanner(System.in);

        System.out.println("/////Below are the default constructor values://///");
        
        Product v1 = new Product();
        System.out.println("\nCode = " + v1.getCode());
        System.out.println("\nDescription = " + v1.getDes()); 
        System.out.println("\nPrice = $" + v1.getPrice());
        
        System.out.println("\n/////Below are user-entered valuses://///");

        System.out.println("\nCode: ");
        c = input.nextLine();

        System.out.println("Description: ");
        d = input.nextLine();

        System.out.println("Price: ");
        p = input.nextDouble();

        Product v2 = new Product(c, d, p);
        System.out.println("\nCode = " + v2.getCode());
        System.out.println("Description: " + v2.getDes());
        System.out.println("Price: $" + v2.getPrice());

        System.out.println("\n/////Below using setter methods to pass literal values, then print() method to display values://///");
        v2.setCode("xyz789");
        v2.setDes("Test Widget");
        v2.setPrice(89.99);
        v2.print();
    }
}