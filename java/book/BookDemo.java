import java.util.Scanner;

class BookDemo {

    public static void main(String[] args){

        String c = "";
        String d = "";
        double p = 0;
        String a = "John Doe";
        Scanner input = new Scanner(System.in);

        System.out.println("\n/////Below are *base class default constructor* values (instantiating v1, then using getter methods)://///");
        
        Product v1 = new Product();
        System.out.println("\nCode = " + v1.getCode());
        System.out.println("\nDescription = " + v1.getDes()); 
        System.out.println("\nPrice = $" + v1.getPrice());
        
        System.out.println("\n/////Below are *base class* user-entered values (instantiating v2, then using getter methods)://///");

        System.out.print("\nCode: ");
        c = input.nextLine();

        System.out.print("Description: ");
        d = input.nextLine();

        System.out.print("Price: ");
        p = input.nextDouble();

        Product v2 = new Product(c, d, p);
        System.out.println("\nCode = " + v2.getCode());
        System.out.println("Description: " + v2.getDes());
        System.out.println("Price: $" + v2.getPrice());

        System.out.println("\n/////Below using setter methods to pass literal values to v2, then print() method to display values://///");
        v2.setCode("xyz789");
        v2.setDes("Test Widget");
        v2.setPrice(89.99);
        v2.print();

        System.out.println("\n/////Below are *derived class default constructor* values (instantiating b1, then using getter methods)://///");

        Book b1 = new Book();
        System.out.println("\nCode = " + b1.getCode());
        System.out.println("Description: " + b1.getDes());
        System.out.println("Price: " + b1.getPrice());
        System.out.println("Author: " + b1.getAuthor());

        System.out.println("\nOr using overridden derived class print() method...");
        b1.print();

        System.out.println("\n/////Below are *derived class default constructor* values (instantiating b2, then using getter methods)://///");

        System.out.print("\nCode: ");
        c = input.next();

        System.out.print("Description: ");
        d = input.next();

        System.out.print("Price: ");
        p = input.nextDouble();
        input.nextLine();

        System.out.print("Author: ");
        a = input.nextLine();

        Book b2 = new Book(c, d, p, a);

        System.out.println("\nCode = " + b2.getCode());
        System.out.println("Description: " + b2.getDes());
        System.out.println("Price: " + b2.getPrice());
        System.out.println("Author: " + b2.getAuthor());

        System.out.println("\nor using derived class print() method...");
        b2.print();
    }
}