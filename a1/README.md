# LIS4331 Advanced Mobile Applications Development

## Steve Walter

### Assignment 1 Requirements:

*four Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Bitbucket repo links: a) This assignment and b) The completed tutorials above.
4. Questions

| README.md file should include the following items:    | Git commands w/short descriptions:                                                       |
|:---                                                   |:---                                                                                      |
| * Screenshot of running JDK java Hello                | 1. git init - Creates a new local repository                                             |
| * Screenshot of running Android Studio - My First App | 2. git status - Lists the files you've changed and those you still need to add or commit |
| * Screenshot of running Android Studio - Contacts App | 3. git add - Adds one or more files to staging                                           |
| * Git commands with short descriptions                | 4. git commit - Commits the changes to the head                                          |
| * Bitbucket repo links                                | 5. git push - Sends the changes to the master branch of remote repository                |
|                                                       | 6. git pull - Fetch and merge changes on the remote server to your working directory     |
|                                                       | 7. git clone - Creates a working of a local repository                                   |

#### Assignment Screenshots:

| Screenshot of running java Hello                           | Screenshot of Android Studio - My First App                   |
|:----------------------------------------------------------:|:-------------------------------------------------------------:|
| ![JDK Installation Screenshot](img/javass.png)             | ![Android Studio Installation Screenshot](img/myfirstapp.jpg) |
| Screenshot of Android Studio - Contacts App First Activity | Screenshot of Android Studio - Contacts App Second Activity   |
| ![Android Studio Contacts App Screenshot](img/img1.jpg)    | ![Android Studio Contacts App Screenshot](img/img2.jpg)       |