package com.example.slw16c.tipcalculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    double numPeople = 0.00;
    double tipAmount = 0.00;
    double billTotal = 0.00;
    double totalTip = 0.00;
    double firstNum = 0.00;
    double totalCost = 0.00;
    String group = "";
    String tipNum = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText total = (EditText)findViewById(R.id.txtBillTotal);
        final Spinner people = (Spinner)findViewById(R.id.txtNum);
        final Spinner tip = (Spinner)findViewById(R.id.txtTip);
        Button amount = (Button)findViewById(R.id.btnResult);
        amount.setOnClickListener(new View.OnClickListener() {
            final TextView result = ((TextView)findViewById(R.id.txtFinal));
            @Override

            public void onClick(View v) {

                group = people.getSelectedItem().toString();
                if(group.equals("1")){
                    numPeople=1.0;
                }else if(group.equals("2")){
                    numPeople=2.0;
                }else if(group.equals("3")){
                    numPeople=3.0;
                }else if(group.equals("4")){
                    numPeople=4.0;
                }else if(group.equals("5")){
                    numPeople=5.0;
                }else if(group.equals("6")){
                    numPeople=6.0;
                }else if(group.equals("7")){
                    numPeople=7.0;
                }else if(group.equals("8")){
                    numPeople=8.0;
                }else if(group.equals("9")){
                    numPeople=9.0;
                }else if(group.equals("10")){
                    numPeople=10.0;
                }

                tipNum = tip.getSelectedItem().toString();
                if (tipNum.equals("0%")) {
                    tipAmount=0;
                }else if(tipNum.equals("5%")){
                    tipAmount=0.05;
                }else if(tipNum.equals("15%")){
                    tipAmount=0.15;
                }else if(tipNum.equals("20%")){
                    tipAmount=0.20;
                }else if(tipNum.equals("25%")){
                    tipAmount=0.25;
                }


                billTotal = Double.parseDouble(total.getText().toString());
                firstNum = tipAmount * billTotal;
                totalTip = firstNum / numPeople;
                totalCost = ((billTotal / numPeople) + totalTip);
                NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.US);
                result.setText("Cost for each of " + group +" guests: " + nf.format(totalCost));




            }
        });

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

    }
}
