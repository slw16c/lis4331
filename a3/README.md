# LIS4331 Advanced Mobile Applications Development

## Steve Walter

### Assignment 3 Requirements:

*two Parts:*

1. Development of Currency Conversion
2. Questions

#### README.md file should include the following items:

* Course title, your name, assignment requirements, as per A1
* Screenshot of running application’s unpopulated user interface
* Screenshot of running application’s toast notification
* Screenshot of running application’s converted currency user interface

#### Assignment Screenshots:

| *Screenshot of running application’s splash screen*   | *Screenshot of running application’s toast notification*    |
|-------------------------------------------------:|:--------------------------------------------------|
| ![Tip Calculator First Activity](img/ss4.jpg) | ![Tip Calculator Second Activity](img/ss2.jpg) |

| *Screenshot of running application’s unpopulated user interface*   | *Screenshot of running application’s converted currency user interface* |
|-------------------------------------------------:|:--------------------------------------------------|
| ![Tip Calculator First Activity](img/ss1.jpg) | ![Tip Calculator Second Activity](img/ss3.jpg) |
