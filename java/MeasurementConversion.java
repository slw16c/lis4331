import java.util.Scanner;

public class MeasurementConversion {
  public static void main(String[] args) {
   
        //Declare variables

        Scanner input = new Scanner(System.in);

        int num = 0;
        double cent = 0;
        double meter = 0;
        double foot = 0;
        double yard = 0;
        double mile = 0;
        boolean validate;

        System.out.println("Program converts inches to centimeters, meters, feet, yards, and miles");
        System.out.println("***Notes***:");
        System.out.println("1)  Use integer for inches (must validate integer input).");
        System.out.println("2)  Use printf() function to print (format values per below output).");
        System.out.println("3)  Create Java \"constants\" for the following values:");
        System.out.println("\tINCHES_TO_CENTIMETER");
        System.out.println("\tINCHES_TO_METER");
        System.out.println("\tINCHES_TO_FOOT");
        System.out.println("\tINCHES_TO_YARD");
        System.out.println("\tFEET_TO_MILE");



        //Checking to make sure the user only inputs integers
        do{
          System.out.print("Please enter a number of inches: ");
          if (input.hasNextInt()) {
            num = input.nextInt();
            validate = true;
          }else{
            System.out.print("Not a valid integer!\n");
            validate = false;
            input.next();
          }
        }while (!(validate));
        

    //mathy stuff
    cent = num*2.54;
    meter = num*0.0245;
    foot = num*0.0833333;
    yard = num*0.0277778;
    mile = foot*0.000189394;
    
    System.out.printf("%,2d" + " inch(es) eqaul: \n\n" , num);
    System.out.printf("%,.6f" + " centimeter(s)\n" , cent);
    System.out.printf("%,.6f" + " meter(s)\n" , meter);
    System.out.printf("%,.6f" + " feet(s)\n" , foot);
    System.out.printf("%,.6f" + " yard(s)\n" , yard);
    System.out.printf("%,.8f" + " mile(s)" , mile);

  }//end of main
}//end of MeasurementConversion