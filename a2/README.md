# LIS4331 Advanced Mobile Applications Development

## Steve Walter

### Assignment 2 Requirements:

*two parts:*

1. Creating an app that calculates the total of bill and tip split with each person
2. Questions

#### README.md file should include the following items:

* Screenshot of Tip Calculator First Activity
* Screenshot of Tip Calculator Second Activity

#### Assignment Screenshots:

| *Screenshot of Tip Calculator First Activity*   | *Screenshot of Tip Calculator Second Activity*    |
|-------------------------------------------------:|:--------------------------------------------------|
| ![Tip Calculator First Activity](img/tipss1.jpg) | ![Tip Calculator Second Activity](img/tipss2.jpg) |
