import java.io.File;
import java.io.FileWriter;
import java.util.Scanner;

public class FileWriteReadCountWords {
    public static void main(String[] args) {

        String data = "";
        Scanner input  = new Scanner(System.in);
        int num = 0;

        File a = new File("filecountwords.txt");
        FileWriter fr = null;

        System.out.println("Program captures user input, writes to and reads from same file, and counts number of words in file.");
        System.out.println("Hint: use hasNext() method to read number of words (tokens).");
        System.out.print("\nPlease enter text: ");
        data = input.nextLine();


        try {
            fr = new FileWriter(a);
            fr.write(data);
        }catch (Exception e) {
            e.printStackTrace();
        }finally{
            try{
                fr.close();
            }catch (Exception e) {
                e.printStackTrace();
        }}



// Hello how is it going

        String[] words = data.split("\\s+");
        num = words.length;

        System.out.println("Saved to file \"filecountwords.txt\"");
        System.out.println("Number of words: " + num);


    }//end of main
}//end of FileWriteReadCountWords
