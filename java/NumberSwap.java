import java.util.Scanner;

public class NumberSwap {
    public static void main(String[] args) {

       
        boolean lock = true;
        boolean lock2 = true;
        Scanner input = new Scanner(System.in);
        int num1 = 0;
        int num2 = 0;
        String firstInput = "";
        String secondInput = "";


    System.out.println("Program swaps two integers. \nNote: Program checks for integers and non-numeric values.\n");

    while (lock == true){

        System.out.print("Please enter first number: ");
        firstInput = input.next();

        try {
            num1 = Integer.parseInt(firstInput);
            lock = false;
        } catch (Exception e) {
            System.out.print("Not valid integer!\n");
            lock = true;
        }
    }

    while (lock2 == true){

        System.out.print("Please enter second number: ");
        secondInput = input.next();

        try {
            num2 = Integer.parseInt(secondInput);
            lock2 = false;
        } catch (Exception e) {
            System.out.print("Not valid integer!\n");
            lock2 = true;
        }
    }

    System.out.println("\nBefore Swapping\nnum1 = " + num1 + "\nnum2 = " + num2);
    System.out.println("\nAfter Swapping\nnum1 = " + num2 + "\nnum2 = " + num1);


    }//end of main
}//end of NumberSwap
