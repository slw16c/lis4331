import java.util.Scanner;

public class LargestThreeNumber {
    public static void main(String[] args) {

            boolean lock = true;
            boolean lock2 = true;
            boolean lock3 = true;
            Scanner input = new Scanner(System.in);
            double num1 = 0;
            double num2 = 0;
            double num3 = 0;
            String firstInput = "";
            String secondInput = "";
            String thirdInput = "";


        System.out.println("Program evaluates largest of three integers. \nNote: Program checks for integers and non-numeric values.\n");


        System.out.print("Please enter first number: ");

        while (lock == true){

            firstInput = input.next();

            try {
                num1 = Integer.parseInt(firstInput);
                lock = false;
            } catch (Exception e) {
                System.out.print("Not valid integer!\n");
                System.out.print("Please try again. Enter first number: ");
                lock = true;
            }
        }

        System.out.print("\nPlease enter second number: ");

        while (lock2 == true){

            secondInput = input.next();

            try {
                num2 = Integer.parseInt(secondInput);
                lock2 = false;
            } catch (Exception e) {
                System.out.print("Not valid integer!\n");
                System.out.print("Please try again. Enter second number: ");
                lock2 = true;
            }
        }
        
        System.out.print("\nPlease enter third number: ");

        while (lock3 == true){

            thirdInput = input.next();

            try {
                num3 = Integer.parseInt(thirdInput);
                lock3 = false;
            } catch (Exception e) {
                System.out.print("Not valid integer!\n");
                System.out.print("Please try again. Enter third number: ");
                lock3 = true;
            }
        }

        if (num1 > num2){
            if (num1 > num3){
                System.out.println("\nFirst number is largest");
            }else {
                System.out.println("\nThird number is largest");
            }
        }else if (num2 > num1){
            if (num2 > num3){
                System.out.println("\nSecond number is largest");
            }else {
                System.out.println("\nThird number is largest");
            }
        }

    }
}