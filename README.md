# LIS4331 Advanced Mobile Applications Development

## Steve Walter

### LIS4331 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")

    * Install JDK
    * Install Android Studio
    * Provide screenshots of installations
    * Create Bitbucket repo
    * Complete Bitbucket tutorials
    * Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")

    * Create a Tip Calculator app
    * Drop-down menu for total number of guests
    * Drop-down menu for tip percentage
    * Must add background color
    * Must create and displaylauncher icon image

3. [A3 README.md](a3/README.md "My A3 README.md file")

    * Create Currency Conversion app
    * Created splash screen
    * created toast error message

4. [A4 README.md](a4/README.md "My A4 README.md file")

    * Create Mortgage Interest Calculator app
    * Include splash screen image, app title, intro text.
    * Must use persistent data: SharedPreferences
    * Widgets and images must be vertically and horizontally aligned

5. [A5 README.md](a5/README.md "My A5 README.md file")

    * Create RSS Feed viwer app
    * Include splash screen with app title and list of articles
    * Must find and use your own RSS feed
    * Must add background color(s) or theme
    * Create and displaylauncher icon image

6. [P1 README.md](p1/README.md "My P1 README.md file")

    * Created an app that plays music
    * Allow the app to pause the song
    * Makes other buttons invisible
    * Created a Splash Screen

7. [P2 README.md](p2/README.md "My P2 README.md file")

    * Include splash screen.
    * Insert at least five sample tasks
    * Test database class
    * Must add background color(s) or theme
    * Create and display launcher icon image.