import java.util.Scanner;

public class DecisionStructures {
 
 
    public static void main(String[] args) {
   
        //Declare varibles
        String phone = "";
        
        //Declare Scanner object
        Scanner input = new Scanner(System.in);

        System.out.println("Program evaluates user-enterned characters.\nUse following characters: W or w, C or c, H or h, N or n.\nUse following decision structures: if...else, and switch.\n");
        /*System.out.println("Use following characters: W or w, C or c, H or h, N or n.");
        System.out.println("Use following decision structures: if...else, and switch.\n");*/

        System.out.println("Phone types: W or w (work), C or c (cell), H or h (home), N or n (none)");
        System.out.print("Enter phone type: ");
        phone=input.nextLine().toUpperCase();
        //input.close();

        


        System.out.println("\nif...else:");
        if (phone.equals("W")) {
            System.out.println("Phone Type: work");
        } else if(phone.equals("C")){
            System.out.println("Phone Type: cell");
        } else if(phone.equals("H")){
            System.out.println("Phone Type: home");
        } else if(phone.equals("N")){
            System.out.println("Phone Type: none");
        } else {
            System.out.println("Incorrect Entry");
        }

        System.out.println("\nSwitch:");
        switch (phone) {
            case "W": System.out.println("Phone Type: work");break;
            case "C": System.out.println("Phone Type: cell");break;
            case "H": System.out.println("Phone Type: home");break;
            case "N": System.out.println("Phone Type: none");break;
            default : System.out.println("Incorrect Entry");
        }


    }//End of main
}//End of class