# LIS4331 Advanced Mobile Applications Development

## Steve Walter

### Assignment 5 Requirements:

*two Parts:*

1. Create RSS Feed viewer app
2. Questions

#### README.md file should include the following items:

* Screenshot of Main RSS Feed
* Screenshot of Description page  
* Screenshot of Opened Page

#### Assignment Screenshots:

| *Screenshot of Main RSS Feed*   | *Screenshot of Description page*    |  *Screenshot of Opened Page*    |
|-------------------------------------------------:|:--------------------------------------------------:|:--------------------------------|
| ![Screenshot of Main RSS Feed](img/1.jpg) | ![Screenshot of Description page](img/2.jpg) | ![Screenshot of Opened Page](img/3.jpg) |
