import java.util.Scanner;

public class StringCount {
  public static void main(String[] args) {
   
        //Declare variables
        char[] ch;
        String inputText;
		int letter = 0;
		int space = 0;
		int num = 0;
		int other = 0;
        Scanner input = new Scanner(System.in);

        System.out.println("Program count number and types of characters: that is, letters, spaces, numbers, and other characters.");
        System.out.println("Hint: You may find the following mehtods helpful: isLetter(), isDigit(), isSpaceChar().");
        System.out.println("Additionally, you could add the functionality for upper vs lower case letters. ;)");

        System.out.print("Please enter string: ");
        inputText = input.nextLine();
        ch = inputText.toCharArray();
        
		for(int i = 0; i < inputText.length(); i++){
			if(Character.isLetter(ch[i])){
				letter ++ ;
			}
			else if(Character.isDigit(ch[i])){
				num ++ ;
			}
			else if(Character.isSpaceChar(ch[i])){
				space ++ ;
			}
			else{
				other ++;
			}
        }
        
        System.out.println("Your string: " + '"' + inputText + '"' + " has the following number and types of characters:");
        System.out.println("letter(s): " + letter);
        System.out.println("space(s): " + space);
        System.out.println("number(s) " + num);
        System.out.println("other character(s): " + other);

  }//end of main
}//end of StringCount