class Book extends Product {

    private String Author;

    public Book() {

        super();
        System.out.println("\nInside book default constructor.");

        Author = "John Doe";
    }

    public Book(String c, String d, double p, String a) {
        super(c,d,p);

        System.out.println("\nInside book constructor with parameters.");

        Author = a;
    }

    public String getAuthor() {

        return Author;

    }

    public void setAuthor(String a){

        Author = a;

    }

    public void print() {
        super.print();
        System.out.print(", Author: " + Author);
    }












}